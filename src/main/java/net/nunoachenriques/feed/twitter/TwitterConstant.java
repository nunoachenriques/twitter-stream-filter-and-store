/*
 * Copyright 2017 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.feed.twitter;

import java.util.Locale;
import java.util.TimeZone;

/**
 * The Twitter's constant values (e.g., TWITTER_USER_ID_NONE)
 * for configurations and other uses among several classes.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 */
public class TwitterConstant {

    private TwitterConstant() {}

    /** The assets file with the Twitter configuration parameters. */
    static final String TWITTER_PROPERTIES_FILE = "net/nunoachenriques/feed/twitter/local.properties";

    /** The Twitter's minimum length for the screen name. */
    private static final int TWITTER_SCREEN_NAME_MIN = 1;
    /** The Twitter's maximum length for the screen name. */
    private static final int TWITTER_SCREEN_NAME_MAX = 15;
    /**
     * Twitter's screen name pattern rule, includes length checking, i.e.,
     * the regular expression pattern means: {@code \w} for any word
     * (a to z and A to Z and 0 to 9 and _); {@code {1,15}} for length between
     * 1 and 15 characters. https://support.twitter.com/articles/101299
     */
    public static final String TWITTER_SCREEN_NAME_PATTERN = "\\w{"+TWITTER_SCREEN_NAME_MIN+","+TWITTER_SCREEN_NAME_MAX+"}";

    /** Twitter's numeric (long integer) null user id. */
    public static final long TWITTER_USER_ID_NONE = 0L;

    /**
     * Twitter's date pattern "{@code EEE MMM dd HH:mm:ss Z yyyy}"
     * (e.g., <a href="https://dev.twitter.com/rest/reference/post/statuses/update">Wed Sep 05 00:37:15 +0000 2012</a>).
     */
    public static final String TWITTER_DATE_PATTERN = "EEE MMM dd HH:mm:ss Z yyyy";
    /** Twitter's all date's time zone */
    public static final TimeZone TWITTER_TIMEZONE = TimeZone.getTimeZone("UTC");
    /** Twitter's all date's locale */
    public static final Locale TWITTER_LOCALE = Locale.US;
}
