/*
 * Copyright 2017 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.feed.twitter;

import org.pmw.tinylog.Logger;

import java.util.Properties;

import twitter4j.ConnectionLifeCycleListener;
import twitter4j.FilterQuery;
import twitter4j.RateLimitStatus;
import twitter4j.RateLimitStatusEvent;
import twitter4j.RateLimitStatusListener;
import twitter4j.StallWarning;
import twitter4j.Status;
import twitter4j.StatusDeletionNotice;
import twitter4j.StatusListener;
import twitter4j.TwitterStream;
import twitter4j.TwitterStreamFactory;
import twitter4j.conf.Configuration;
import twitter4j.conf.PropertyConfiguration;

/**
 * Listens on Twitter stream, processes the feed, and stores the results in a
 * repository. It uses Twitter4J library.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 * @see TwitterStorage
 * @see <a href="http://twitter4j.org/" target="_blank">Twitter4J</a>
 */
public class Twitter {

    private Configuration configuration;
    private FilterQuery filterQuery;
    private TwitterStream twitterStream;
    private TwitterStorage<TwitterData> twitterStorage;
    private TwitterSentiment twitterSentiment;
    private StatusListener statusListener;

    /**
     * Specific case constructor: follows one user stream. Designed to avoid API
     * external dependency, using Java 1.7 language libraries API.
     * Opens the storage.
     *
     * @param p The Twitter configuration properties.
     * @param uid The Twitter user id of whom to follow.
     * @param tsn The Twitter storage name (uses {@link TwitterStorageSQLite}).
     * @param tsa The Twitter text sentiment analysis.
     * @throws Exception On failure just throws the cause.
     */
    public Twitter(Properties p, long uid, String tsn, TwitterSentiment tsa)
            throws Exception {
        this(new PropertyConfiguration(p), new FilterQuery(uid), new TwitterStorageSQLite(tsn), tsa);
    }

    /**
     * General case constructor.
     * Opens the storage.
     *
     * @param c {@link Configuration} properties.
     * @param fq {@link FilterQuery} with restrictions to stream.
     * @param ts {@link TwitterStorage} for storing status and sentiment data.
     * @param tsa The Twitter text sentiment analysis.
     * @throws Exception On failure just throws the cause.
     */
    public Twitter(Configuration c, FilterQuery fq, TwitterStorage<TwitterData> ts, TwitterSentiment tsa)
            throws Exception {
        configuration = c;
        filterQuery = fq;
        twitterStream = null;
        twitterStorage = ts;
        twitterStorage.open();
        twitterSentiment = tsa;
        statusListener = null;
    }

    /**
     * Sets the Twitter stream feed with the configuration, filter query,
     * status and rate listeners. Also opens the storage for tweets.
     */
    public void stream() {
        twitterStream = new TwitterStreamFactory(configuration).getInstance();
        twitterStream.addConnectionLifeCycleListener(connectionListener());
        twitterStream.addRateLimitStatusListener(rateListener());
        statusListener = statusListener();
        twitterStream.addListener(statusListener);
        twitterStream.filter(filterQuery);
    }

    /**
     * Shuts down the stream dispatcher to avoid having lost threads connected
     * and still streaming from Twitter! Closes the tweets storage.
     */
    public void shutdown() {
        try {
            if (twitterStream != null) {
                twitterStream.clearListeners();
                twitterStream.shutdown();
            }
        } finally {
            if (twitterStorage != null) {
                twitterStorage.close();
            }
        }
    }

    /**
     * Useful for simulation purposes.
     *
     * @return The {@link StatusListener} of this Twitter stream.
     */
    StatusListener getStatusListener() {
        return statusListener;
    }

    /**
     * Useful for simulation purposes.
     *
     * @return The {@link TwitterStorage} of this Twitter stream.
     */
    TwitterStorage<TwitterData> getStorage() {
        return twitterStorage;
    }

    private ConnectionLifeCycleListener connectionListener() {
        return new ConnectionLifeCycleListener() {

            @Override
            public void onConnect() {
                Logger.debug("Stream CONNECTED with filterQuery: {}", filterQuery.toString());
            }

            @Override
            public void onDisconnect() {
                Logger.debug("Stream DISCONNECTED!");
            }

            @Override
            public void onCleanUp() {
                Logger.debug("Stream CLEANUP...!");
            }
        };
    }

    private RateLimitStatusListener rateListener() {
        return new RateLimitStatusListener() {

            @Override
            public void onRateLimitStatus(RateLimitStatusEvent event) {
                RateLimitStatus rls = event.getRateLimitStatus();
                Logger.warn("Status rate limit: [{}] remaining: [{}]", rls.getLimit(), rls.getRemaining());
            }

            @Override
            public void onRateLimitReached(RateLimitStatusEvent event) {
                RateLimitStatus rls = event.getRateLimitStatus();
                Logger.warn("Status rate limit reached, seconds until reset: [{}]", rls.getSecondsUntilReset());
            }
        };
    }

    private StatusListener statusListener() {
        return new StatusListener() {

            @Override
            public void onStatus(Status s) {
                long id = s.getId();
                String text = s.getText();
                Logger.debug("@{}:{}", s.getUser().getScreenName(), text);
                try {
                    Float ss = twitterSentiment.getSentiment(text);
                    if (ss == null || !twitterStorage.store(new TwitterData(s, ss))) {
                        Logger.error("[{}] STATUS AND SENTIMENT NOT STORED!", id);
                    }
                } catch (Exception e) {
                    Logger.error(e, "[{}] STATUS AND SENTIMENT NOT STORED!", id);
                }
            }

            @Override
            public void onDeletionNotice(StatusDeletionNotice sdn) {
                long id = sdn.getStatusId();
                try {
                    if (!twitterStorage.delete(id)) {
                        Logger.error("[{}] STATUS NOT DELETED!", id);
                    }
                } catch (Exception e) {
                    Logger.error(e, "[{}] STATUS NOT DELETED!", id);
                }
            }

            @Override
            public void onTrackLimitationNotice(int numberOfLimitedStatuses) {
                Logger.warn("Stream track limitation notice: {}", numberOfLimitedStatuses);
            }

            @Override
            public void onScrubGeo(long userId, long upToStatusId) {
                Logger.debug("Stream scrub_geo event userId: [{}] upToStatusId: [{}]", userId, upToStatusId);
            }

            @Override
            public void onStallWarning(StallWarning warning) {
                Logger.warn("Stream stall warning: {}", warning);
            }

            @Override
            public void onException(Exception e) {
                Logger.warn("Stream exception: {}", e.getMessage());
            }
        };
    }
}
