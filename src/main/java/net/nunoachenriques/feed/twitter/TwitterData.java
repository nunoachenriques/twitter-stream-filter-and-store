/*
 * Copyright 2017 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.feed.twitter;

import twitter4j.Status;

/**
 * The application Twitter status and sentiment analysis data place holder for
 * storage interchange.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 * @see Status
 */
public class TwitterData {

    private Status status;
    private float sentiment;

    //public TwitterData() {}

    /**
     * Constructor that sets all data.
     *
     * @param s Status (Twitter tweet).
     * @param ss Sentiment (calculated from the status text).
     */
    @SuppressWarnings("WeakerAccess")
    public TwitterData(Status s, float ss) {
        status = s;
        sentiment = ss;
    }

    /*
    public void setStatus(Status s) {
        status = s;
    }

    public void setSentiment(float s) {
        sentiment = s;
    }
    */

    public Status getStatus() {
        return status;
    }

    public float getSentiment() {
        return sentiment;
    }
}
