/*
 * Copyright 2017 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.feed.twitter;

import org.pmw.tinylog.Logger;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.SimpleDateFormat;
import java.util.LinkedList;
import java.util.List;

import twitter4j.Status;
import twitter4j.TwitterObjectFactory;

/**
 * An implementation of TwitterStorage ready to use.
 * To store all data (e.g., tweets AKA status updates).
 * Also guarantees the returned results ordered by date ascending.
 * Thread safe, connection auto closed on failure.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 * @see TwitterData
 */
public final class TwitterStorageSQLite
        implements TwitterStorage<TwitterData> {

    private static final String TWITTER_TABLE = "twitter_data";
    private static final String[] DROP_ALL = {
            "drop table if exists " + TWITTER_TABLE
    };
    private static final String[] CREATE_ALL = {
            "create table " + TWITTER_TABLE + " (id integer not null, timestamp_epoch integer not null, user_id integer not null, screen_name text not null, status text not null, sentiment real not null)",
            "create unique index " + TWITTER_TABLE + "__id___index on " + TWITTER_TABLE + "(id)",
            "create index " + TWITTER_TABLE + "__timestamp_epoch__user_id___index on " + TWITTER_TABLE + "(timestamp_epoch,user_id)"
    };
    private static final SimpleDateFormat twitterDateFormat;
    static {
        twitterDateFormat = new SimpleDateFormat(TwitterConstant.TWITTER_DATE_PATTERN, TwitterConstant.TWITTER_LOCALE);
        twitterDateFormat.setTimeZone(TwitterConstant.TWITTER_TIMEZONE);
    }

    private static Connection connection;
    private static PreparedStatement storePS;
    private static PreparedStatement getPS;
    private static PreparedStatement getByDatePS;
    private static PreparedStatement deletePS;
    private static PreparedStatement deleteByDatePS;

    private final String name;

    /**
     * Sets the storage (database) name.
     * For a memory database just use ":memory:"
     *
     * @param s Storage (database) name (e.g., "/tmp/twitter.db", ":memory:").
     */
    TwitterStorageSQLite(String s) {
        name = s;
        connection = null;
    }

    @Override
    public void open()
            throws Exception {
        Logger.debug("os.name: {}. os.arch: {}. java.runtime.name: {}.", System.getProperty("os.name"), System.getProperty("os.arch"), System.getProperty("java.runtime.name"));
        if (org.sqlite.util.OSInfo.isAndroid()) {
            Class.forName("org.sqlite.JDBC");
        }
        synchronized (TwitterStorageSQLite.class) {
            connection = DriverManager.getConnection(String.format("jdbc:sqlite:%s", name));
            try {
                if (!isCreated(connection)) {
                    setCreateAll(connection);
                }
            } catch (Exception e) {
                connection.close();
                throw new Exception(e.getMessage(), e.getCause());
            }
        }
    }

    private boolean isCreated(Connection c)
            throws Exception {
        boolean r = false;
        DatabaseMetaData dmd = c.getMetaData();
        try (ResultSet rs = dmd.getTables(null, null, TWITTER_TABLE, null)) {
            if (rs.next()) {
                r = true;
            }
        }
        return r;
    }

    private void setDropAll(Connection c)
            throws SQLException {
        int[] dar = executeSQLList(DROP_ALL, c);
        if (dar.length == 0) {
            String m = "setDropAll FAILS! executeSQLList(DROP_ALL) returned nothing!";
            Logger.error(m);
            throw new SQLException(m);
        }
    }

    private void setCreateAll(Connection c)
            throws SQLException {
        int[] car = executeSQLList(CREATE_ALL, c);
        if (car.length == 0) {
            String m = "setCreateAll FAILS! executeSQLList(CREATE_ALL) returned nothing!";
            Logger.error(m);
            throw new SQLException(m);
        } else {
            storePS = c.prepareStatement(String.format("insert into %s values (?,?,?,?,?,?)", TWITTER_TABLE));
            getPS = c.prepareStatement(String.format("select * from %s where id = ? limit 1", TWITTER_TABLE));
            getByDatePS = c.prepareStatement(String.format("select * from %s where timestamp_epoch between ? and ?", TWITTER_TABLE));
            deletePS = c.prepareStatement(String.format("delete from %s where id = ?", TWITTER_TABLE));
            deleteByDatePS = c.prepareStatement(String.format("delete from %s where timestamp_epoch between ? and ?", TWITTER_TABLE));
        }
    }

    private int[] executeSQLList(String[] sqlList, Connection c)
            throws SQLException {
        try (Statement s = c.createStatement()) {
            for (String sql : sqlList) {
                s.addBatch(sql);
            }
            return s.executeBatch();
        }
    }

    @Override
    public void close() {
        try {
            if (connection != null) {
                connection.close();
            }
        } catch (SQLException e) {
            Logger.error(e.getMessage());
        }
    }

    @Override
    public void clean()
            throws Exception {
        try {
            synchronized (TwitterStorageSQLite.class) {
                setDropAll(connection);
                setCreateAll(connection);
            }
        } catch (SQLException e) {
            Logger.error("CLOSING CONNECTION... clean FAILS! {}", e.getMessage());
            this.close();
            throw new SQLException(e.getMessage(), e.getCause());
        }
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public boolean store(TwitterData td)
            throws Exception {
        synchronized (TwitterStorageSQLite.class) {
            Status s = td.getStatus();
            storePS.setLong(1, s.getId());
            storePS.setLong(2, javaToSQLiteTime(s.getCreatedAt().getTime()));
            storePS.setLong(3, s.getUser().getId());
            storePS.setString(4, s.getUser().getScreenName());
            storePS.setString(5, s.getText());
            storePS.setFloat(6, td.getSentiment());
            return (storePS.executeUpdate() == 1);
        }
    }

    @Override
    public TwitterData get(long id) {
        synchronized (TwitterStorageSQLite.class) {
            TwitterData td = null;
            try {
                getPS.setLong(1, id);
                try (ResultSet rs = getPS.executeQuery()) {
                    if (rs.next()) {
                        Status s = JSONToStatus(statusDataToJSON(rs));
                        if (s != null) {
                            td = new TwitterData(s, rs.getFloat(6));
                        }
                    }
                }
            } catch (SQLException e) {
                Logger.error("get FAILS! {}", e.getMessage());
            }
            return td;
        }
    }

    @Override
    public List<TwitterData> get(long fromDate, long toDate) {
        synchronized (TwitterStorageSQLite.class) {
            List<TwitterData> tdl = new LinkedList<>();
            try {
                getByDatePS.setLong(1, javaToSQLiteTime(fromDate));
                getByDatePS.setLong(2, javaToSQLiteTime(toDate));
                try (ResultSet rs = getByDatePS.executeQuery()) {
                    while (rs.next()) {
                        Status s = JSONToStatus(statusDataToJSON(rs));
                        if (s != null) {
                            tdl.add(new TwitterData(s, rs.getFloat(6)));
                        }
                    }
                }
            } catch (SQLException e) {
                Logger.error("get (by date) FAILS! {}", e.getMessage());
            }
            return tdl;
        }
    }

    @Override
    public boolean delete(long id)
            throws Exception {
        synchronized (TwitterStorageSQLite.class) {
            deletePS.setLong(1, id);
            return (deletePS.executeUpdate() == 1);
        }
    }

    @Override
    public boolean delete(long fromDate, long toDate)
            throws Exception {
        synchronized (TwitterStorageSQLite.class) {
            deleteByDatePS.setLong(1, javaToSQLiteTime(fromDate));
            deleteByDatePS.setLong(2, javaToSQLiteTime(toDate));
            return (deleteByDatePS.executeUpdate() > 0);
        }
    }

    /**
     * <a href="https://dev.twitter.com/overview/api/tweets" target="_blank">Twitter
     * JSON format</a> to Twitter4J Status object.
     *
     * @param json A JSON format string of Twitter status data.
     * @return A {@link Status} object or {@code null} on failure.
     * @see #statusDataToJSON(long, String, long, String, String)
     * @see Status
     */
    static Status JSONToStatus(String json) {
        Status s = null;
        try {
            s = TwitterObjectFactory.createStatus(json);
        } catch (Exception e) {
            Logger.error("Fails on creating status object! Hint: rawJSON well formed? {}", e.getMessage());
        }
        return s;
    }

    private String statusDataToJSON(ResultSet rs)
            throws SQLException {
        return statusDataToJSON(
                rs.getLong(1),
                twitterDateFormat.format(sqliteToJavaTime(rs.getLong(2))),
                rs.getLong(3),
                rs.getString(4),
                rs.getString(5)
        );
    }

    /**
     * <a href="https://dev.twitter.com/overview/api/tweets" target="_blank">Twitter
     * JSON</a> snippet of interest for returning values:
     *
     * <pre>
     * {
     * "id":64-bit int (Java long),
     * "created_at":"UTC US locale format",
     * "user":{"id":64-bit int (Java long),"screen_name":""},
     * "text":"UTF-8 text of the tweet"
     * }
     * </pre>
     *
     * @param id Status (Tweet) id.
     * @param createdAt Date of status creation in Twitter UTC US locale format
     *                  "{@code EEE MMM dd HH:mm:ss Z yyyy}"
     *                  (e.g., <a href="https://dev.twitter.com/rest/reference/post/statuses/update" target="_blank">Wed Sep 05 00:37:15 +0000 2012</a>).
     * @param userId User long id.
     * @param screenName User screen name.
     * @param text Status text.
     * @return A JSON formatted text string.
     */
    static String statusDataToJSON(long id, String createdAt, long userId, String screenName, String text) {
        return "{" +
                "\"id\":" + id + "," +
                "\"created_at\":\"" + createdAt + "\"," +
                "\"user\":{\"id\":" + userId + ",\"screen_name\":\"" + screenName + "\"}," +
                "\"text\":\"" + text + "\"" +
                "}";
    }

    /**
     * Converts Java milliseconds since UNIX Epoch to SQLite seconds since UNIX
     * Epoch.
     *
     * @param milliseconds Milliseconds since UNIX Epoch.
     * @return Seconds since UNIX Epoch.
     */
    private static long javaToSQLiteTime(long milliseconds) {
        return milliseconds/1000L;
    }

    /**
     * Converts SQLite seconds since UNIX Epoch to Java milliseconds since UNIX
     * Epoch.
     *
     * @param seconds Seconds since UNIX Epoch.
     * @return Milliseconds since UNIX Epoch.
     */
    private static long sqliteToJavaTime(long seconds) {
        return seconds*1000L;
    }
}
