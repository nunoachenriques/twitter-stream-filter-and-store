/*
 * Copyright 2018 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.feed.twitter;

/**
 * The Twitter sentiment analysis interface.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 */
public interface TwitterSentiment {

    /**
     * Gets the sentiment in a polarity scale [-1, 1].
     *
     * @param text Text sample for analysis.
     * @return Sentiment polarity [-1, 1]. {@code null} on failure.
     */
    Float getSentiment(String text);
}
