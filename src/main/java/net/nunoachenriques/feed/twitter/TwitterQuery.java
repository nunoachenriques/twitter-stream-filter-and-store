/*
 * Copyright 2017 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.feed.twitter;

import org.pmw.tinylog.Logger;

import java.util.Properties;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.conf.Configuration;
import twitter4j.conf.PropertyConfiguration;

/**
 * Executes queries on Twitter. It uses Twitter4J library.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 * @see <a href="http://twitter4j.org/" target="_blank">Twitter4J</a>
 */
public final class TwitterQuery {

    private Twitter twitter;

    /**
     * Constructor with the Twitter configuration {@link Properties}
     * (avoiding API external dependency).
     *
     * @param p The configuration properties.
     */
    @SuppressWarnings("WeakerAccess")
    public TwitterQuery(Properties p) {
        this(new PropertyConfiguration(p));
    }

    private TwitterQuery(Configuration c) {
        twitter = new TwitterFactory(c).getInstance();
    }

    /**
     * Gets Twitter user id from the specified screen name.
     *
     * @param screenName Twitter user's screen name.
     * @return The user id. {@link TwitterConstant#TWITTER_USER_ID_NONE} on failure.
     */
    public long getUserId(String screenName) {
        long userId = TwitterConstant.TWITTER_USER_ID_NONE;
        try {
            userId = twitter.showUser(screenName).getId();
        } catch (TwitterException e) {
            Logger.error("getUserId FAILED! {}", e.getMessage());
        }
        return userId;
    }
}
