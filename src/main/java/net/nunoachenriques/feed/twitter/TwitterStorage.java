/*
 * Copyright 2017 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.feed.twitter;

import java.util.List;

/**
 * The application storage interface.
 * To store all data (e.g., tweets AKA status updates and resulting analysis).
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 */
public interface TwitterStorage<R> {

    /**
     * Opens storage for read and write.
     *
     * @throws Exception On failure just throws the cause.
     */
    void open() throws Exception;

    /**
     * Closes the opened storage.
     */
    void close();

    /**
     * Cleans the opened storage, i.e., removes all data.
     *
     * @throws Exception On failure just throws the cause.
     */
    void clean() throws Exception;

    /**
     * Gets the storage name (e.g., database name if SQLite).
     *
     * @return The storage name.
     */
    String getName();

    /**
     * Stores one data record.
     *
     * @param r The data record to be stored.
     * @return True if stored, false otherwise.
     * @throws Exception On failure just throws the cause.
     */
    boolean store(R r) throws Exception;

    /**
     * Gets one specific status identified by {@code id}.
     *
     * @param id Status unique id.
     * @return Status data, {@code null} if data not found or error.
     */
    R get(long id);

    /**
     * Gets zero or more data records from the specified time interval from
     * {@code fromDate} to {@code toDate}. The result must be ordered by date
     * ascending.
     *
     * @param fromDate Beginning of date interval
     *                 (Java milliseconds since UNIX Epoch UTC).
     * @param toDate End of date interval
     *               (Java milliseconds since UNIX Epoch UTC).
     * @return A list of zero or more data records ordered by date ascending,
     *         empty if none.
     * @throws Exception On failure just throws the cause.
     */
    List<R> get(long fromDate, long toDate) throws Exception;

    /**
     * Removes the status identified by {@code id} from the storage.
     *
     * @param id Status id.
     * @return True if removed, false otherwise.
     * @throws Exception On failure just throws the cause.
     */
    boolean delete(long id) throws Exception;

    /**
     * Deletes zero or more data records from the specified time interval from
     * {@code fromDate} to {@code toDate}.
     *
     * @param fromDate Beginning of date interval
     *                 (Java milliseconds since UNIX Epoch UTC).
     * @param toDate End of date interval
     *               (Java milliseconds since UNIX Epoch UTC).
     * @return True if removed, false otherwise.
     * @throws Exception On failure just throws the cause.
     */
    boolean delete(long fromDate, long toDate) throws Exception;
}
