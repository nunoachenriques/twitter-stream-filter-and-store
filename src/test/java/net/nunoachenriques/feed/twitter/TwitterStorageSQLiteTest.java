/* 
 * Copyright 2017 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.feed.twitter;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.pmw.tinylog.Configurator;
import org.pmw.tinylog.Level;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import junitparams.FileParameters;
import junitparams.JUnitParamsRunner;
import junitparams.mappers.CsvWithHeaderMapper;
import twitter4j.Status;

/**
 * Testing the {@code TwitterStorage} implementation
 * ({@code TwitterStorageSQLite}) functionality!
 * It's a unit test for the class.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 * @see TwitterStorageSQLite
 */
@RunWith(JUnitParamsRunner.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public final class TwitterStorageSQLiteTest {

    private static final String DATABASE = ":memory:"; // (e.g., ":memory:", "/tmp/twitterstoragesqlitetest.db")
    private static TwitterStorage<TwitterData> twitterStorage;
    private static SimpleDateFormat simpleDateFormat;
    private static TwitterSentiment twitterSentiment;

    /**
     * Instantiates and opens the TwitterStorage repository.
     *
     * @throws Exception On failure just throws the cause.
     */
    @BeforeClass
    public static void beforeAllOnlyOnce() throws Exception {
        Configurator.defaultConfig().level(Level.DEBUG).activate();
        simpleDateFormat = new SimpleDateFormat(TwitterConstant.TWITTER_DATE_PATTERN, TwitterConstant.TWITTER_LOCALE);
        simpleDateFormat.setTimeZone(TwitterConstant.TWITTER_TIMEZONE);
        twitterSentiment = new TwitterSentiment() {
            @Override
            public Float getSentiment(String text) {
                return 0.0f; // dummy value
            }
        };
        twitterStorage = new TwitterStorageSQLite(DATABASE);
        twitterStorage.open();
        twitterStorage.clean();
    }

    /**
     * Tests conversions between data to JSON to {@link Status} object.
     */
    @Test
    @FileParameters(value = "classpath:net/nunoachenriques/feed/twitter/twitter-samples.csv", mapper = CsvWithHeaderMapper.class)
    public void testADataJSONStatusConversion(long id, String createdAt, long userId, String screenName, String text) {
        String statusJSON = TwitterStorageSQLite.statusDataToJSON(id, createdAt, userId, screenName, text);
        Status statusSet = TwitterStorageSQLite.JSONToStatus(statusJSON);
        String gotText = statusSet.getText();
        Assert.assertEquals("Original text: [" + text + "] DIFFERENT FROM Got text: [" + gotText + "]!", text, gotText);
        String gotDate = simpleDateFormat.format(statusSet.getCreatedAt());
        Assert.assertEquals("Original date: [" + createdAt + "] DIFFERENT FROM Got date: [" + gotDate + "]!", createdAt, gotDate);
    }

    @Test
    @FileParameters(value = "classpath:net/nunoachenriques/feed/twitter/twitter-samples.csv", mapper = CsvWithHeaderMapper.class)
    public void testBStoreAndGetAndDeleteStatus(long id, String createdAt, long userId, String screenName, String text) throws Exception {
        // STORE
        String statusJSON = TwitterStorageSQLite.statusDataToJSON(id, createdAt, userId, screenName, text);
        Status statusSet = TwitterStorageSQLite.JSONToStatus(statusJSON);
        boolean stored = twitterStorage.store(new TwitterData(statusSet, 0.0f)); // NOT testing sentiment here.
        Assert.assertTrue(String.format("[%d] STATUS NOT STORED!", id), stored);
        // GET
        Status statusGet = twitterStorage.get(id).getStatus();
        String gotText = statusGet.getText();
        Assert.assertEquals("Original text: [" + text + "] DIFFERENT FROM Got text: [" + gotText + "]!", text, gotText);
        String gotDate = simpleDateFormat.format(statusGet.getCreatedAt());
        Assert.assertEquals("Original date: [" + createdAt + "] DIFFERENT FROM Got date: [" + gotDate + "]!", createdAt, gotDate);
        // DELETE
        boolean deleted = twitterStorage.delete(id);
        Assert.assertTrue(String.format("[%d] STATUS NOT DELETED!", id), deleted);
    }

    @Test
    @FileParameters(value = "classpath:net/nunoachenriques/feed/twitter/twitter-samples.csv", mapper = CsvWithHeaderMapper.class)
    public void testDStoreAndSearchStatusAndSentiment(long id, String createdAt, long userId, String screenName, String text) throws Exception {
        // STORE
        String statusJSON = TwitterStorageSQLite.statusDataToJSON(id, createdAt, userId, screenName, text);
        Status statusSet = TwitterStorageSQLite.JSONToStatus(statusJSON);
        Float ts = twitterSentiment.getSentiment(text);
        Assert.assertNotNull(ts);
        boolean stored = twitterStorage.store(new TwitterData(statusSet, ts));
        Assert.assertTrue(String.format("[%d] STATUS AND SENTIMENT NOT STORED!", id), stored);
        // GET/SEARCH
        // time search interval including one status created date in the middle.
        try {
            long date = simpleDateFormat.parse(createdAt).getTime();
            long fromDate = date - 3600000L; // 1 hour before.
            long toDate = date + 3600000L; // 1 hour after.
            List<TwitterData> statusGet = twitterStorage.get(fromDate, toDate);
            int quantityGot = statusGet.size();
            int quantityExpected;
            switch ((int)id) {
                case 1:
                    quantityExpected = 1;
                    break;
                case 2:
                    quantityExpected = 2;
                    break;
                case 3:
                    quantityExpected = 3;
                    break;
                case 4:
                case 5:
                case 6:
                default:
                    quantityExpected = 1;
                    break;
            }
            Assert.assertEquals("Status quantity expected: [" + quantityExpected + "] DIFFERENT FROM found: [" + quantityGot + "]!", quantityExpected, quantityGot);
        } catch (ParseException e) {
            throw new AssertionError(e);
        }
    }

    @Test
    public void testEDeleteAll() throws Exception {
        boolean deleted = twitterStorage.delete(0, System.currentTimeMillis());
        Assert.assertTrue("DELETED ALL FAILED!", deleted);
    }

    /**
     * TwitterStorage close.
     */
    @AfterClass
    public static void afterAllOnlyOnce() {
        if (twitterStorage != null) {
            twitterStorage.close();
        }
    }
}
