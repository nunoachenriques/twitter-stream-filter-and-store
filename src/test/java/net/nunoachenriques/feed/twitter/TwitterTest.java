/* 
 * Copyright 2017 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.feed.twitter;

import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.pmw.tinylog.Configurator;
import org.pmw.tinylog.Level;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import junitparams.FileParameters;
import junitparams.JUnitParamsRunner;
import junitparams.mappers.CsvWithHeaderMapper;
import twitter4j.Status;
import twitter4j.StatusListener;

/**
 * Testing the Twitter functionality! It's a unit test for the class,
 * also an integration test for the TwitterStorage, sentiment and language.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 * @see Twitter
 */
@RunWith(JUnitParamsRunner.class)
public final class TwitterTest {

    private static final long TWITTER_USER_ID = 855056749015040002L; // nach_empa_d1
    private static final String DATABASE = ":memory:"; // (e.g., ":memory:", "/tmp/twittertest.db")
    private static Twitter twitter;
    private static TwitterStorage<TwitterData> twitterStorage;
    private static StatusListener statusListener;

    /**
     * Instantiates Twitter stream.
     * Tests getting configuration properties, Twitter storage.
     * Moreover, gets the status listener for simulation purposes.
     */
    @BeforeClass
    public static void beforeAllOnlyOnce()
            throws Exception {
        Configurator.defaultConfig().level(Level.DEBUG).activate();
        TwitterSentiment twitterSentiment = new TwitterSentiment() {
            @Override
            public Float getSentiment(String text) {
                return 0.0f; // dummy value
            }
        };
        twitter = new Twitter(getPropertyConfiguration(), TWITTER_USER_ID, DATABASE, twitterSentiment);
        twitterStorage = twitter.getStorage();
        twitterStorage.clean();
        twitter.stream();
        statusListener = twitter.getStatusListener();
    }

    private static Properties getPropertyConfiguration() throws IOException {
        Properties p = new Properties();
        InputStream is = TwitterTest.class.getClassLoader().getResourceAsStream(TwitterConstant.TWITTER_PROPERTIES_FILE);
        p.load(is);
        is.close();
        return p;
    }

    /**
     * Tests storing a status got by {@link StatusListener} from a Twitter user
     * and its calculated sentiment.
     */
    @Test
    @FileParameters(value = "classpath:net/nunoachenriques/feed/twitter/twitter-samples.csv", mapper = CsvWithHeaderMapper.class)
    public void testOnStatus(long id, String createdAt, long userId, String screenName, String text) {
        String statusJSON = TwitterStorageSQLite.statusDataToJSON(id, createdAt, userId, screenName, text);
        Status statusSet = TwitterStorageSQLite.JSONToStatus(statusJSON);
        statusListener.onStatus(statusSet);
        TwitterData tdGet = twitterStorage.get(id);
        String gotText;
        try {
            gotText = tdGet.getStatus().getText();
        } catch (Exception e) {
            gotText = "";
        }
        Assert.assertEquals("Original text: [" + text + "] DIFFERENT FROM Got text: [" + gotText + "]!", text, gotText);
    }

    /**
     * Twitter stream shutdown.
     */
    @AfterClass
    public static void afterAllOnlyOnce() {
        if (twitter != null) {
            twitter.shutdown();
        }
    }
}
