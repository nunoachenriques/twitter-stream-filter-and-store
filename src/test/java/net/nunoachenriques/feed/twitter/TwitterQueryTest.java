/* 
 * Copyright 2017 Nuno A. C. Henriques [nunoachenriques.net]
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package net.nunoachenriques.feed.twitter;

import org.junit.Assert;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.pmw.tinylog.Configurator;
import org.pmw.tinylog.Level;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import junitparams.JUnitParamsRunner;
import junitparams.Parameters;

/**
 * Testing the Twitter functionality! It's a unit test for the class.
 *
 * @author Nuno A. C. Henriques [nunoachenriques.net]
 * @see TwitterQuery
 */
@RunWith(JUnitParamsRunner.class)
public final class TwitterQueryTest {

    private static TwitterQuery twitter;

    /**
     * Instantiates {@link TwitterQuery}.
     * Tests getting configuration properties.
     *
     * @throws Exception On failure just throws the cause.
     */
    @BeforeClass
    public static void beforeAllOnlyOnce() throws Exception {
        Configurator.defaultConfig().level(Level.DEBUG).activate();
        twitter = new TwitterQuery(getProperties());
    }

    private static Properties getProperties() throws IOException {
        Properties p = new Properties();
        InputStream is = TwitterQueryTest.class.getClassLoader().getResourceAsStream(TwitterConstant.TWITTER_PROPERTIES_FILE);
        p.load(is);
        is.close();
        return p;
    }

    /**
     * Tests getting a Twitter user id from a screen name.
     */
    @Test
    @Parameters({"nach_empa_d1, 855056749015040002"})
    public void testGetUserId(String screenName, long userId) {
        long gotUserId = twitter.getUserId(screenName);
        Assert.assertEquals("Given user id: [" + userId + "] DIFFERENT FROM Got user id: [" + gotUserId + "]!", userId, gotUserId);
    }
}
