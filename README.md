[![Apache 2.0 License](https://img.shields.io/badge/license-Apache%202.0-blue.svg "Apache 2.0 License")][1]

# Twitter Stream Filter and Store

Testing Twitter stream filter with [Twitter4J](https://github.com/yusuke/twitter4j) and local SQLite store.

## Repository

https://gitlab.com/nunoachenriques/twitter-stream-filter-and-store

## Gradle

This project includes the [Gradle](https://gradle.org/) wrapper for the common
PC operating systems Linux and macOS (`gradlew`) and Windows (`gradlew.bat`).
The included build (e.g., compiling, testing, releasing) tool should be used
with no need for further dependencies.

## Development

To integrate with Twitter:

 1. Go to https://apps.twitter.com/.
 2. Apply for your app access and generate:
    1. Consumer key and secret.
    2. Access token and secret.
 3. Create `src/main/resources/net/nunoachenriques/feed/twitter/local.properties`
    and fill it with your generated app and access key, token and secrets.

```properties
debug = true
loggerFactory = twitter4j.StdOutLoggerFactory
includeRTs = false
includeEntities = false
includeMyRetweet = false
oauth.consumerKey =
oauth.consumerSecret =
oauth.accessToken =
oauth.accessTokenSecret =
```

 4. Take care and keep these tokens secret (at least the access ones).
    This project `.gitignore` already has a rule for this file.

## Test

NOTICE: one test needs OAuth properly configured, check [Development](#development).

```shell
./gradlew test
```

The test results report may be checked at
`build/reports/tests/test/index.html`.

## Install

```shell
./gradlew installDist
```

The released packages for distribution are at **`build/install/`**.

## Troubleshooting

### Java 1.7 compatibility

1. Install OpenJDK 7.
2. Suffix Gradle command-line with `-Dorg.gradle.java.home=/path_to_jdk_7` such as (Debian GNU/Linux):

```shell
./gradlew installDist -Dorg.gradle.java.home=/usr/lib/jvm/java-7-openjdk-amd64/
```

## License

Copyright 2017 Nuno A. C. Henriques [http://nunoachenriques.net/]

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

[1]: http://www.apache.org/licenses/LICENSE-2.0.html
